import {
    SoundTouch,
    SimpleFilter,
    getWebAudioNode
} from "/node_modules/soundtouchjs/dist/soundtouch.js";

export function initTimeStrecher(wavesurfer) {

    // Time stretcher
    wavesurfer.on('ready', function () {
        const st = new SoundTouch(
            wavesurfer.backend.ac.sampleRate
        );
        const buffer = wavesurfer.backend.buffer;
        const channels = buffer.numberOfChannels;
        const l = buffer.getChannelData(0);
        const r = channels > 1 ? buffer.getChannelData(1) : l;
        const length = buffer.length;
        let seekingPos = null;
        let seekingDiff = 0;

        const source = {
            extract: function (target, numFrames, position) {
                if (seekingPos != null) {
                    seekingDiff = seekingPos - position;
                    seekingPos = null;
                }

                position += seekingDiff;

                for (let i = 0; i < numFrames; i++) {
                    target[i * 2] = l[i + position];
                    target[i * 2 + 1] = r[i + position];
                }

                return Math.min(numFrames, length - position);
            }
        };

        let soundtouchNode;

        wavesurfer.on('play', function () {
            seekingPos = ~~(wavesurfer.backend.getPlayedPercents() * length);
            st.tempo = wavesurfer.getPlaybackRate();

            if (st.tempo === 1) {
                wavesurfer.backend.disconnectFilters();
            } else {
                if (!soundtouchNode) {
                    const filter = new SimpleFilter(source, st);
                    soundtouchNode = getWebAudioNode(
                        wavesurfer.backend.ac,
                        filter
                    );
                }
                wavesurfer.backend.setFilter(soundtouchNode);
            }
        });

        wavesurfer.on('pause', function () {
            soundtouchNode && soundtouchNode.disconnect();
        });

        wavesurfer.on('seek', function () {
            seekingPos = ~~(wavesurfer.backend.getPlayedPercents() * length);
        });
    });
}