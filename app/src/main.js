import {initTimeStrecher} from "./timestrecher.filter.js";

window.addEventListener("DOMContentLoaded", () => {

    const lyricsContainer = document.getElementById("lyrics_container");
    const colors = ["rgba(0, 0, 255, 0.1)", "rgba(0, 255, 0, 0.1)", "rgba(0, 255, 255, 0.1)", "rgba(255, 0, 0, 0.1)"];

    const minZoom = 50;
    const maxZoom = 500;

    let currentZoom = -1;
    let currentCursorProgress = -1;

    let lyricEntries = [];
    let selectedEntry = null;
    let lastUpdateTask = null;

    // Init waveform
    const wavesurfer = WaveSurfer.create({
        container: '#waveform',
        waveColor: 'violet',
        progressColor: 'purple',
        responsive: true,
        pixelRatio: 1,
        partialRender: false,
        hideScrollbar: true,
        plugins: [
            WaveSurfer.cursor.create({
                showTime: true
            }),
            WaveSurfer.minimap.create({
                container: "#minimap",
                waveColor: '#000',
                progressColor: 'black',
                height: 30,
                showOverview: true,
                barWidth: 0,
                barGap: 0,
                barRadius: false,
                overviewBorderColor: "azure",
            }),
            WaveSurfer.regions.create({
                dragSelection: false
            }),
            WaveSurfer.timeline.create({
                container: "#timeline"
            })
        ]
    });
    initTimeStrecher(wavesurfer);
    wavesurfer.zoom(50);
    wavesurfer.load("/app/public/data/Ben Moon ft Veela - Majesty.mp3");

    wavesurfer.on("error", message => {
        console.error(message);
    });
    wavesurfer.on("seek", progress => {
        currentCursorProgress = progress;
    });
    wavesurfer.on("region-created", region => {
        const label = region.attributes.label || null;

        if (label) {
            const labelElement = document.createElement("div");
            labelElement.classList.add("region-label");
            labelElement.innerText = label;
            region.element.appendChild(labelElement);
        }
    });

    // Add listener on input file
    const inputFile = document.getElementById("file_input");
    inputFile.addEventListener("change", () => {
        const files = inputFile.files;
        if (files.length > 0) {
            const file = files[0];
            wavesurfer.loadBlob(file);
        }
    });

    // Add listener on play/pause button
    const playButton = document.getElementById("play_btn");
    playButton.addEventListener("click", () => {
        togglePlay();
    });

    // Add listener on rate slider
    const rateSlider = document.getElementById("rate_slider");
    rateSlider.addEventListener("input", event => {
        wavesurfer.setPlaybackRate(event.target.value);
    });

    // Add scroll listener on waveform
    const waveformContainer = document.querySelector("#waveform > wave");
    waveformContainer.addEventListener("wheel", event => {
        const lastZoom = currentZoom;
        currentZoom = Math.max(minZoom, Math.min(maxZoom, currentZoom - event.deltaY * 10));
        if (lastZoom !== currentZoom)
            wavesurfer.zoom(currentZoom);
    });

    // Add listener to save button
    const saveButton = document.getElementById("save_btn");
    saveButton.addEventListener("click", () => saveState());

    // Add global key event
    window.addEventListener("keydown", event => {
        switch (event.which) {
            case 32:
                togglePlay();
                break;

            case 90:
                if (event.ctrlKey) {
                    markLyric();
                    return false
                }
                break;
        }
    });

    function formatTimestamp(timestampInSec) {
        const timestampInMs = timestampInSec * 1000;
        const minutes = Math.floor(timestampInSec / 60);
        const seconds = Math.floor(timestampInSec % 60);
        const milliseconds = Math.floor(timestampInMs - (minutes * 60 * 1000) - (seconds * 1000));

        const minutesStr = minutes < 10 ? "0" + minutes : minutes;
        const secondsStr = seconds < 10 ? "0" + seconds : seconds;
        let fillingStr;
        if (milliseconds < 10)
            fillingStr = "00";
        else if (milliseconds < 100)
            fillingStr = "0";
        else
            fillingStr = "";
        let millisecondsStr = `${fillingStr}${milliseconds}`;

        return `${minutesStr}:${secondsStr}.${millisecondsStr}`;
    }

    function saveState() {
        const entries = lyricEntries
            .map(entry => {
                const region = entry.region;
                const forgedEntry = {
                    timestamp: entry.timestamp,
                    lyric: entry.lyric
                };
                if (region) {
                    forgedEntry.region = {
                        id: region.id,
                        start: region.start,
                        end: region.end,
                        color: region.color
                    }
                }
                return forgedEntry;
            });

        const state = {
            lastCursorProgress: wavesurfer.getCurrentTime() / wavesurfer.getDuration(),
            selectedEntryIndex: lyricEntries.indexOf(selectedEntry),
            entries: entries
        };

        localStorage.setItem("state", JSON.stringify(state));
    }

    function addLyricEntry(index, lyric, timestamp = null) {
        const row = document.createElement("div");
        row.dataset.index = index;
        row.classList.add("row");
        if (!timestamp && index !== 0)
            row.classList.add("disabled");
        row.addEventListener("click", () => {
            selectEntry(index);
        });
        lyricsContainer.appendChild(row);

        const timestampColumn = document.createElement("div");
        timestampColumn.classList.add("timestamp");
        if (timestamp)
            timestampColumn.innerText = formatTimestamp(timestamp);
        row.appendChild(timestampColumn);

        const lyricColumn = document.createElement("div");
        lyricColumn.classList.add("lyric");
        lyricColumn.innerText = lyric;
        row.appendChild(lyricColumn);

        lyricEntries.push({
            timestamp: timestamp,
            lyric: lyric,
            rowElement: row
        });
    }

    function loadFromSnapshot() {
        const storedState = localStorage.getItem("state");
        if (!storedState)
            return false;

        const state = JSON.parse(storedState);

        state.entries.forEach((entry, index) => {
            addLyricEntry(index, entry.lyric, entry.timestamp);
        });

        selectEntry(state.selectedEntryIndex);

        wavesurfer.once("ready", () => {
            wavesurfer.seekAndCenter(state.lastCursorProgress);
            state.entries.forEach((entry) => {
                if (entry.region) {
                    const savedRegion = entry.region;
                    const lyricEntry = lyricEntries[savedRegion.id];
                    const restoredRegion = addRegion(
                        savedRegion.id,
                        savedRegion.start,
                        savedRegion.end,
                        savedRegion.color,
                        entry.lyric
                    );
                    restoredRegion.data.lyricEntry = lyricEntry;
                    lyricEntry.region = restoredRegion;
                }
            })
        });

        return true;
    }

    function loadLyrics(lyrics, timestamps = null) {
        const lyricLines = lyrics.split("\n");
        lyricLines.forEach((line, index) => {
            const timestamp = timestamps ? timestamps[index] : null;
            addLyricEntry(index, line, timestamp);
        });
    }

    function selectEntry(entryIndex) {
        if (entryIndex < 0 || entryIndex >= lyricEntries.length)
            return;

        if (selectedEntry) {
            selectedEntry.rowElement.classList.remove("selected");
            if (lyricEntries.indexOf(selectedEntry) >= entryIndex)
                stopRegionUpdate();
        }

        selectedEntry = lyricEntries[entryIndex];
        selectedEntry.rowElement.classList.add("selected");

        const duration = wavesurfer.getDuration();
        if (selectedEntry.timestamp) {
            wavesurfer.seekAndCenter(selectedEntry.timestamp / duration);
        }
    }

    function getPreviousEntry() {
        const selectedIndex = lyricEntries.indexOf(selectedEntry);
        const prevIndex = selectedIndex - 1;

        if (prevIndex >= 0)
            return lyricEntries[prevIndex];
        else
            return null;
    }

    function getNextEntry() {
        const selectedIndex = lyricEntries.indexOf(selectedEntry);
        const nextIndex = selectedIndex + 1;

        if (nextIndex < lyricEntries.length)
            return lyricEntries[nextIndex];
        else
            return null;
    }

    function selectNextEntry() {
        selectEntry(selectedEntry.region.id + 1);
    }

    // Load lyrics
    if (!loadFromSnapshot()) {
        loadLyrics(lyricsData);
        selectEntry(0);
    }

    function togglePlay() {
        if (wavesurfer !== null) {
            if (wavesurfer.isPlaying()) {
                wavesurfer.pause();
                if (currentCursorProgress >= 0)
                    wavesurfer.seekTo(currentCursorProgress);
            } else
                wavesurfer.play();
        }
    }

    function updateLyricEntry(entry) {
        if (!entry)
            return;

        const timestampColumn = entry.rowElement.querySelector(".timestamp");
        timestampColumn.innerText = formatTimestamp(entry.timestamp);
    }

    function addRegion(id, start, end, color, label) {
        const region = wavesurfer.addRegion({
            id: id,
            start: start,
            end: end,
            loop: false,
            drag: false,
            resize: true,
            color: color,
            attributes: {
                label: label
            }
        });
        region.onResize = (delta, direction) => {
            const regions = wavesurfer.regions.list;
            if (direction === 'start') {
                // Get previous region
                const prevRegion = regions[region.id - 1];
                const minStart = prevRegion ? prevRegion.start + 0.4 : 0;
                const currentStart = region.start;
                const newStart = currentStart + delta;

                region.update({
                    start: Math.max(newStart, minStart)
                });

                if (newStart <= prevRegion.end || currentStart === prevRegion.end)
                    prevRegion.update({
                        end: newStart
                    });
            } else {
                // Get next region
                const nextRegion = regions[region.id + 1];
                const maxEnd = nextRegion ? nextRegion.end - 0.4 : wavesurfer.getDuration();
                const currentEnd = region.end;
                const newEnd = currentEnd + delta;

                region.update({
                    end: Math.min(newEnd, maxEnd)
                });

                if (newEnd >= nextRegion.start || currentEnd === nextRegion.start)
                    nextRegion.update({
                        start: newEnd
                    });
            }

        };
        region.on("in", () => {
            region.element.classList.add("highlight");
        });
        region.on("out", () => {
            region.element.classList.remove("highlight");
        });
        region.on("update", () => {
            const lyricEntry = region.data.lyricEntry;
            lyricEntry.timestamp = region.start;
            updateLyricEntry(lyricEntry);
        });

        return region;
    }

    function markLyric() {
        const currentTime = wavesurfer.getCurrentTime();

        // If cursor is in current or neighborhood selected entry region, correct timestamp
        const currentRegion = wavesurfer.regions.getCurrentRegion();
        const selectedEntryRegion = selectedEntry.region;
        const prevEntry = getPreviousEntry();
        if (
            currentRegion !== null && currentRegion.data.lyricEntry === selectedEntry ||
            (
                (prevEntry !== null && prevEntry.region.start < currentTime || prevEntry === null) &&
                (selectedEntryRegion !== undefined && currentTime < selectedEntryRegion.end)
            )
        ) {
            selectedEntryRegion.update({
                start: currentTime
            });

            if (prevEntry) {
                prevEntry.region.update({
                    end: currentTime
                });
            }
        }
        // If we need to mark next region
        else if (selectedEntry.timestamp === null && (currentRegion === null || currentRegion.end === currentTime)) {

            const entryBeforeIndex = lyricEntries.indexOf(selectedEntry) - 1;
            const entryBefore = entryBeforeIndex >= 0 ? lyricEntries[entryBeforeIndex] : null;
            const regionBefore = entryBefore ? entryBefore.region : null;
            const regionBeforeEnd = regionBefore ? regionBefore.end : currentTime;
            const entryAfterIndex = entryBeforeIndex + 2;
            const entryAfter = entryAfterIndex < lyricEntries.length ? lyricEntries[entryAfterIndex] : null;
            const regionAfter = entryAfter.region;
            const regionAfterStart = regionAfter ? regionAfter.start : -1;
            const lyric = selectedEntry.lyric;
            const regionId = entryBeforeIndex + 1;
            const color = colors[regionId % colors.length];

            const regionStart = regionBefore ? regionBeforeEnd : currentTime;
            const regionEnd = regionAfterStart >= 0 ? regionAfterStart : currentTime;

            const region = addRegion(regionId, regionStart, regionEnd, color, lyric);

            stopRegionUpdate();
            startRegionUpdate(region);

            region.data.lyricEntry = selectedEntry;
            selectedEntry.region = region;
            selectedEntry.timestamp = currentTime;
            updateLyricEntry(selectedEntry);

            selectNextEntry();
            selectedEntry.rowElement.classList.remove("disabled");
        }
    }

    function stopRegionUpdate() {
        if (lastUpdateTask != null) {
            clearInterval(lastUpdateTask);
            lastUpdateTask = null;
        }
    }

    function startRegionUpdate(region) {
        lastUpdateTask = setInterval(() => {
            region.update({
                end: wavesurfer.getCurrentTime()
            });
        }, 25);
    }

    function outputLyricFormat() {
        return lyricEntries.reduce(
            (buffer, entry) => {
                const timestamp = entry.timestamp;
                const formatedTimestamp = timestamp ? `[${formatTimestamp(timestamp)}]` : "";

                return `${buffer}\n${formatedTimestamp}${entry.lyric}`
            },
            ""
        );
    }

    function printOutputLyricFormat() {
        console.log(outputLyricFormat());
    }

    window.printOutputLyricFormat = printOutputLyricFormat;
});